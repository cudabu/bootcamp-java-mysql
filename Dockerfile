FROM openjdk:8-alpine

ENV DB_SERVER=
ENV DB_NAME=
ENV DB_USER=user
ENV DB_PWD=password

EXPOSE 8080

RUN mkdir -p /home/app

# set default dir so that next commands executes in /home/app dir
WORKDIR /home/app

# copy JAR to container
COPY ./build/libs/bootcamp-java-mysql-project-1.0-SNAPSHOT.jar /home/app/app.jar

# will execute JAR in /home/app because of WORKDIR
CMD java -jar app.jar